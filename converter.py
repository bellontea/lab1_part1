import argparse


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--dollar")
    parser.add_argument("--ruble")
    parser.add_argument("--index", default=60)
    parser.add_argument("--file", default='output.txt')
    args = parser.parse_args()
    with open(args.file, "w") as file:
        if args.dollar:
            print(f"Ruble {int(args.dollar) * int(args.index)}")
            file.write(f"Ruble {int(args.dollar) * int(args.index)}")
        elif args.ruble:
            print(f"Dollar {int(args.ruble) / int(args.index)}")
            file.write(f"Dollar {int(args.ruble) / int(args.index)}")
        else:
            print("Input dollar or ruble for convert")
            file.write("Input dollar or ruble for convert")
